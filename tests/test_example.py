import pytest
from django.http import HttpResponse

from cmsapp.models import Page

@pytest.fixture
def pages(request, db):
    return [
        Page.objects.create(title='ma-ta', content='tac-tu'),
        Page.objects.create(title='handicapat', content='tac-tu'),
        Page.objects.create(title='concentrare', content='tac-tu'),
    ]

@pytest.fixture
def authed_clinet(db, client, user):
    client.force_login(user)

@pytest.fixture
def client(request, client):
    func = client.request

    def wrapper(**kwargs):
        print('>>>>', ' '.join('{}={!r}'.format(*item)
                               for item in kwargs.items()))
        resp = func(**kwargs)
        print('<<<<', resp, resp.content)
        return resp
    client.request = wrapper
    return client

def test_empty_index(client, db):
    resp = client.get('/')
    assert resp.status_code == 200
    
    assert len(resp.context['pages']) == 0
    content = resp.content.decode(resp.charset)
    assert 'No pages'in content

def test_index(pages, client):
    resp = client.get('/')
    assert resp.status_code == 200
    assert list(resp.context['pages']) == pages
    content = resp.content.decode(resp.charset)
    for page in pages:
        assert page.title in content

def test_show(client, pages):
    myurl = '/' + pages[0].slug.decode('UTF-8') + '/'
    resp = client.get(myurl)
    assert resp.status_code == 200
    assert resp.context['page'] == pages[0]
    content = resp.content.decode(resp.charset)
    assert 'tac-tu' in content

def test_show_redirect(pages, client):
    myurl = '/' + pages[0].slug.decode('UTF-8')
    resp = client.get(myurl)
    assert resp.status_code == 301
    assert resp.url == myurl + '/'

def test_pageadmin(pages, client, admin_user):
    client.force_login(admin_user)
    resp = client.get('/admin/cmsapp/page/')
    assert resp.status_code == 200

def test_delete(pages, client):
    myurl = '/' + pages[0].slug.decode('UTF-8') + '/delete'
    resp = client.get(myurl)
    assert resp.status_code == 302
    assert resp.url == '/'

def test_add(db, client):
    resp = client.post('/+', {'title': 'Foo',
                             'tags': 'idk',
                              'tags': 't1,t 2',
                             'content': 'Baaaar'})
    content = resp.content.decode(resp.charset)
    print(content)
    assert resp.status_code == 302
    location = resp._headers['location']
    assert resp.url == location[1]
    resp = client.post(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'Foo' in content
    assert 'Baaaar' in content
    assert 'href="/:t1/"' in content
    assert 'href="/:t%202/"' in content

@pytest.mark.parametrize('data', [
    {'title': '', 'content': 'lorem'},
    {'title': 'titlu', 'content': ''},
])
def test_add_empty(db, client, data):
    resp = client.post('/+', data)
    content = resp.content.decode(resp.charset)
    assert resp.status_code == 200
    assert 'This field is required' in content

def test_edit(pages, client):
    myslug = pages[0].slug.decode('UTF-8')
    myurl = '/' + myslug + '/edit/'
    resp = client.get(myurl)
    assert resp.status_code == 200
    data = resp.context['form'].initial
    data['title'] = 'noua ma-ta'


    resp = client.post(myurl, data)
    assert resp.status_code == 302
    assert resp.url == '/' + myslug + '/'

    resp = client.post(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'noua ma-ta' in content