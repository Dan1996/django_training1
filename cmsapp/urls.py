from django.conf.urls import url
from . import views

app_name ='cmsapp'

urlpatterns =[
    url(r'^(?P<slug>[^/]+)/delete$', views.delete, name='delete'),
    url(r'^$', views.index, name='index'),
    url(r'^:(?P<tag>[^/]+)/$', views.index, name='index'),
    url(r'^(?P<slug>[^/]+)/$', views.page, name='page'),
    url(r'^(?P<slug>[^/]+)/edit/$', views.change, name='change'),
    url(r'^\+$', views.change, name='change'),
]