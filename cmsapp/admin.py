# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Page
from django.contrib import admin
# Register your models here.


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = '__str__', 'id', 'title', 'trimmed_content'
    list_editable = ['title']

    def trimmed_content(self, obj):
        return obj.content[:10]

    trimmed_content.admin_order_field = 'content'
    trimmed_content.short_description = 'Content'
