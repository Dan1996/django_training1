# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from binascii import hexlify

from django.db import models
from os import urandom

def make_random_slug():
    return hexlify(urandom(6))

class Page(models.Model):
    def __repr__(self):  # https://pyformat.info/
        return "<Page #{0.pk} slug={0.slug!r} title={0.title!r}" \
               " content={0.content!r:.5}>".format(self)
    title = models.CharField(max_length=200)
    content = models.TextField()
    slug = models.SlugField(max_length=200, unique=True, default=make_random_slug)

class Tag(models.Model):
    name = models.CharField(max_length=200)
    page = models.ForeignKey("Page",
                             on_delete=models.CASCADE,
                             related_name='tags',
                             related_query_name='tag')
