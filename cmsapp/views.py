# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect, resolve_url

from .forms import PageForm
from .models import Page, Tag


# Create your views here.

def index(request, tag=None):
    if tag is None:
        pages = Page.objects.all()
    else:
        pages = Page.objects.filter(tag__name=tag)

    return render(request, "cmsapp/tem.html", {'pages': pages,
                                               'tags': Tag.objects.values('name').distinct(),})


def page(request, slug):
    page = get_object_or_404(Page, slug=slug)
    return render(request, "cmsapp/page.html", {
        'page': page,
        'tags': page.tags.all(),
    })

def delete(request, slug):
    instance = get_object_or_404(Page, slug=slug)

    if request.method == 'POST':
        instance.delete()
        return redirect('cmsapp:index')
    return render(request, 'cmsapp/delete.html', {
         'page': instance,
    })


# def add(request):
#     if request.method == 'POST':
#         form = PageForm(request.POST)
#         if form.is_valid():
#             obj = form.save()
#             return redirect('cmsapp:page', slug=obj.slug)
#     else:
#         form = PageForm()
#     return render(request, 'cmsapp/add.html', {
#         'form': form,
#     })


def change(request, slug=None):
    if slug is None:
        instance = None
    else:
        instance = get_object_or_404(Page, slug=slug)
    if request.method == "POST":
        form = PageForm(request.POST, instance=instance)
        if form.is_valid():
            instance = form.save()
            return redirect("cmsapp:page", slug=instance.slug)
    else:
        form = PageForm(instance=instance)
    return render(request, 'cmsapp/change.html', {
        'form': form, 'page': instance,
    })


# def edit(request, slug):
#     instance = get_object_or_404(Page, slug=slug)
#     if request.method == 'POST':
#         form = PageForm(request.POST, instance=instance)
#         if form.is_valid():
#             form.save()
#             return redirect('cmsapp:page', slug=instance.slug)
#     else:
#         form = PageForm(instance=instance)
#
#     return render(request, 'cmsapp/edit.html', {
#         'form': form,
#         'page': instance
#     })